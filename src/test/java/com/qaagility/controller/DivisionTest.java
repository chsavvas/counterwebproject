package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class DivisionTest {
    @Test
    public void testD() throws Exception {

        int k= new Division().divide(4,2);
        assertEquals("Divide", 2, k);

    }
    @Test
    public void testZero() throws Exception {

        int z= new Division().divide(4,0);
        assertEquals("Max", Integer.MAX_VALUE, z);

    }

}
